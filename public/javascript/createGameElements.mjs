import { createElement, addClass } from "./helper.mjs";
import { username, onClickJoinRoomButton } from "./game.mjs";
import { MAX_PROGRESS_BAR_WIDTH } from "./config.mjs";

const readyButton = document.getElementById("game-window__button");

export const createRoomCard = ({ roomName, joinedPlayers }) => {
  const roomCard = createElement({
    tagName: "div",
    className: "room-cards__item flex-centered",
    attributes: { id: roomName },
  });

  const countJoined = createElement({
    tagName: "div",
    className: "room-cards__count-joined",
    attributes: { id: "count-joined-players" },
  });
  countJoined.innerText = `${joinedPlayers} joined`

  const roomCardName = createElement({
    tagName: "div",
    className: "room-cards__name",
  });
  roomCardName.innerText = roomName;

  const roomCardButton = createElement({
    tagName: "button",
    className: "room-cards__button",
  });
  roomCardButton.innerText = "JOIN";
  roomCardButton.addEventListener("click", function () {
    onClickJoinRoomButton(roomName);
  });

  roomCard.append(countJoined);
  roomCard.append(roomCardName);
  roomCard.append(roomCardButton);

  return roomCard;
};

export const createPlayerCard = (user) => {
  const playerCard = createElement({
    tagName: "div",
    className: "players__item",
  });

  const playerNameWrapper = createElement({
    tagName: "div",
    className: "players__item-name-wrapper",
  });

  const playerStatus = createElement({
    tagName: "div",
    className: "players__item-status",
  });
  const readyClass = user.ready ? "ready" : "not-ready";
  addClass(playerStatus, readyClass);

  const playerName = createElement({
    tagName: "div",
    className: "players__item-name"
  });
  playerName.innerText = user.username === username ? `${username}  (you)` : user.username;

  const progressBarWrapper = createElement({
    tagName: "div",
    className: "players__item-progress-bar-wrapper",
  });

  const progressBar = createElement({
    tagName: "div",
    className: "players__item-progress-bar",
    attributes: { style: `width: ${user.progressBarWidth}px` },
  });

  if (user.progressBarWidth === MAX_PROGRESS_BAR_WIDTH) { addClass(progressBar, "done"); }

  // TODO: move this line somewhere else
  if (user.username === username) {
    readyButton.innerText = user.ready ? "not ready" : "ready";
  }

  playerNameWrapper.append(playerStatus);
  playerNameWrapper.append(playerName);
  progressBarWrapper.append(progressBar);
  playerCard.append(playerNameWrapper);
  playerCard.append(progressBarWrapper);

  return playerCard;
};
