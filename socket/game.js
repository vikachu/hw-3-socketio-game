import * as config from "./config";
import { texts } from "../public/javascript/data.mjs";
import {
  getLeaderBoard,
  checkAllPlayerReady,
  checkAllPlayersDone,
} from "./gameHelpers";

let rooms = {};
let usernames = [];

export default (io) => {
  io.on("connection", (socket) => {
    const username = socket.handshake.query.username;
    initNewUserConnection(socket, username);

    socket.on("disconnect", function () {
      const index = usernames.indexOf(username);
      usernames.splice(index, 1);

      const roomName = getConnectedUserRoomName(username);
      removeUserFromRoom(username);

      if (roomName !== undefined && checkAllPlayerReady(rooms[roomName])) {
        startCountdown(roomName);
      }
      socket.broadcast.emit("INIT_ROOMS", rooms); // to updated joined counter
    });

    socket.on("CREATE_ROOM", (roomName) => {
      if (rooms[roomName] !== undefined) {
        socket.emit("ROOM_NAME_EXISTS", roomName);
      } else {
        rooms[roomName] = [
          {
            username: username,
            ready: false,
            progressBarWidth: 0,
            timeFinished: 0,
          },
        ];
        const users = rooms[roomName];
        const joinedPlayers = users.length;

        socket.emit("APPEND_ROOM_CARD", { roomName, joinedPlayers });
        socket.broadcast.emit("APPEND_ROOM_CARD", { roomName, joinedPlayers });
        socket.join(roomName, () => {
          socket.emit("JOIN_ROOM_DONE", { roomName, users });
        });
      }
    });

    socket.on("JOIN_ROOM", (roomName) => {
      if (rooms[roomName].length === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        socket.emit("ROOM_IS_FULL", config.MAXIMUM_USERS_FOR_ONE_ROOM);
      } else {
        const user = {
          username: username,
          ready: false,
          progressBarWidth: 0,
          timeFinished: 0,
        };
        rooms[roomName].push(user);
        const users = rooms[roomName];

        socket.join(roomName, () => {
          socket.emit("JOIN_ROOM_DONE", { roomName, users });
          socket.broadcast.emit("UPDATE_ROOM_PLAYERS", { roomName, users });
          socket.broadcast.emit("INIT_ROOMS", rooms);
        });
      }
    });

    socket.on("BACK_TO_ROOMS", (roomName) => {
      removeUserFromRoom(username);

      if (rooms.hasOwnProperty(roomName)) {
        const users = getConnectedUsers(roomName);

        io.to(roomName).emit("PLAYER_JEFT", { roomName, users });
      }
      socket.emit("INIT_ROOMS", rooms);
      socket.broadcast.emit("INIT_ROOMS", rooms);
      socket.leave(roomName);
    });

    socket.on("READY_PRESSED", (roomName) => {
      let users = getConnectedUsers(roomName);
      const userReady = !users.find((user) => user.username === username).ready;
      Object.assign(
        rooms[roomName].find((user) => user.username === username),
        { ready: userReady }
      );

      users = getConnectedUsers(roomName);
      io.to(roomName).emit("READY_PRESSED_DONE", { roomName, users });

      if (checkAllPlayerReady(users)) {
        startCountdown(roomName);
      }
    });

    const startCountdown = (roomName) => {
      let countdownTime = config.SECONDS_TIMER_BEFORE_START_GAME;

        const countdown = setInterval(function () {
          io.to(roomName).emit("COUNTDOWN_BEFORE_START", {
            roomName,
            countdownTime,
          });

          countdownTime--;
          if (countdownTime < 0) {
            clearInterval(countdown);
            startGame(roomName);
          }
        }, 1000);
    };

    const startGame = (roomName) => {
      let timeForGame = config.SECONDS_FOR_GAME;
      const textIndex = Math.floor(Math.random() * texts.length);
      let users = getConnectedUsers(roomName);

      io.to(roomName).emit("START_GAME", { roomName, textIndex, timeForGame });
      timeForGame--; // to reduce delay

      const gameTimer = setInterval(function () {
        io.to(roomName).emit("GAME_TIMER", { roomName, timeForGame });

        timeForGame--;
        if (timeForGame < 0 || checkAllPlayersDone(users)) {
          clearInterval(gameTimer);
          const leaderBoard = getLeaderBoard(users);

          clearUserProgress(roomName);
          users = getConnectedUsers(roomName);

          io.to(roomName).emit("GAME_RESULTS", {
            roomName,
            users,
            leaderBoard,
          });
        }
      }, 1000);
    };

    socket.on("RIGHT_CHAR_TYPED", ({ roomName, progressBarWidth }) => {
      const user = rooms[roomName].find((user) => user.username === username);
      user.progressBarWidth = progressBarWidth;
      user.timeFinished = Date.now();

      const users = getConnectedUsers(roomName);
      io.to(roomName).emit("UPDATE_PROGRESS_BAR", { roomName, users });
    });
  });
};

// need access to global variable rooms and usernames
const getConnectedUsers = (roomName) => {
  return rooms[roomName];
};

const getConnectedUserRoomName = (username) => {
  return Object.keys(rooms).find((roomName) =>
    rooms[roomName].some((user) => user.username === username)
  );
};

const removeUserFromRoom = (username) => {
  for (const roomName in rooms) {
    if (rooms.hasOwnProperty(roomName)) {
      rooms[roomName] = rooms[roomName].filter(
        (user) => user.username !== username
      );
      if (rooms[roomName].length === 0) {
        deleteRoom(roomName);
      }
    }
  }
};

const deleteRoom = (roomName) => {
  delete rooms[roomName];
};

const checkUsernameExists = (username) => {
  return usernames.includes(username);
};

const initNewUserConnection = (socket, username) => {
  if (checkUsernameExists(username)) {
    socket.emit("USERNAME_EXISTS");
  } else {
    usernames.push(username);
    socket.emit("INIT_ROOMS", rooms);
  }
};

const clearUserProgress = (roomName) => {
  rooms[roomName] = rooms[roomName].map((user) => ({
    ...user,
    ready: false,
    progressBarWidth: 0,
    timeFinished: 0,
  }));
};
