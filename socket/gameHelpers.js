import { MAX_PROGRESS_BAR_WIDTH } from "../public/javascript/config.mjs";

export const getLeaderBoard = (users) => {
  let sortedUsers = users;
  sortedUsers.sort(function (a, b) {
    if (a.progressBarWidth === b.progressBarWidth) {
      return a.timeFinished > b.timeFinished ? 1 : -1;
    }
    return a.progressBarWidth < b.progressBarWidth ? 1 : -1;
  });

  const leaderBoard = sortedUsers.map((user) => user.username);
  return leaderBoard;
};

export const checkAllPlayersDone = (users) => {
  return users.every(
    (user) => user.progressBarWidth === MAX_PROGRESS_BAR_WIDTH
  );
};

export const checkAllPlayerReady = (users) => {
  return users.every((user) => user.ready === true);
};
